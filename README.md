# MoroExample

Aplikace vytvořená pro firmu MoroSystems v rámci vstupního pohovoru.
Jedná se o jednoduchou CRUD aplikaci pro správu uživatelů.
Pro operaci mazání je nutné přihlášení, kde zabezpečení je zajištěno pomocí Spring Security.
Pro chod aplikace je třeba PostgreSQL DB, která se inicializuje skriptem init.sql.


Aplikace se skládá z front-end (Angular) a back-end (Java + Spring + Hibernate) části.
Každá část se nachází ve vlastní složce a obsahuje další informace.