import React, { Component } from 'react';
import './App.css';
import Table from './components/Table';
import EditUser from "./components/EditUser";
import superagent from 'superagent';
import Base64 from 'base-64';


class App extends Component {


    constructor(){
        super();
        this.state = {
            users: [
            ],
            isHidden: true,
            userToEdit:
                {
                    name: "Dummy",
                    surname: "Dummy",
                    id: 0
                }
        }

    }



    componentDidMount(){
        this.getTableData();
    }

    getTableData()
    {
        const url = '/users';

        superagent.get(url).then((result) => {
            this.setState({users: result.body});
        })
    }




    deleteRow(id)
    {

        superagent.get("/secured/deleteUser/"+id)
            .then(() => {
                this.getTableData();
            }

    );


    }

    addUser(user)
    {

        let newUsers = this.state.users;

        superagent.post('/user/adduser').send({ id:null, name: user.name, surname: user.surname, username: user.username, password: user.password }).then(
            (result) => {

                newUsers.push(result.body);

                this.setState({
                    users: newUsers
                })

            }
        );
    }

    editUser(userIdToEdit)
    {
        if(userIdToEdit)
        {
            this.setState({
                users: this.state.users,
                isHidden: false,
                userToEdit: this.state.users.find((x => x.id === userIdToEdit))
            })
        }
    }

    cancelEdit()
    {
        this.setState({
            users: this.state.users,
            isHidden: true,
            userToEdit: this.state.userToEdit
        })
    }

    saveChanges(user)
    {

        let newUsers = this.state.users;

        superagent.post('/user/edit').send(user).then(
            (result) => {
                let index = newUsers.indexOf(newUsers.find((x => x.id === user.id)));
                newUsers[index] = result.body;

                this.setState({
                    users: newUsers,
                    isHidden: true,
                })

            }
        );

    }

    render() {

        return (
            <div className="App">
                <Table editUser={this.editUser.bind(this)} addUser={this.addUser.bind(this)} deleteRow={this.deleteRow.bind(this)} data={this.state.users}/>
                {!this.state.isHidden && <EditUser saveChanges={this.saveChanges.bind(this)} cancelEdit={this.cancelEdit.bind(this)} user={this.state.userToEdit}/>}
            </div>
        );
    }
}

export default App;
