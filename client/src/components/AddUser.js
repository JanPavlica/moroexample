import React, { Component } from 'react';
import TextField from "./TextField";


class AddUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: ' ',
            surname: ' ',
            username: '',
            password: ''
        };
    }

    addUser(e){
        e.preventDefault();
        let newUser = this.state;
        newUser["username"] = this.usernameInput.value;
        newUser["password"] = this.passwordInput.value;
        this.props.addUser(this.state);
    }

    onChange(user) {
        let newUser = this.state;
        newUser[user.col.toString()] = user.value;
        this.setState(newUser);
    }


    render() {

        return (
            <div>
                <form onSubmit={this.addUser.bind(this)}>
                    Name:<br/>
                    <TextField col={"name"} onChange={this.onChange.bind(this)}/> <br/>
                    Surname:<br/>
                    <TextField col={"surname"} onChange={this.onChange.bind(this) }/> <br/>
                    Username: <br/><input ref={(usernameInput) => { this.usernameInput = usernameInput }} type="text" name="username"/>   <br/>
                    Password: <br/><input ref={(passwordInput) => { this.passwordInput = passwordInput }} type="password" name="password"/>   <br/>
                    <button>
                        Přidej
                    </button>
                </form>
            </div>

        );
    }
}

export default AddUser;
