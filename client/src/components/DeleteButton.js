import React, { Component } from 'react';


class DeleteButton extends Component {

    handleButton(){
        this.props.deleteRow();
    }

    render() {

        return (
            <td>
                <button onClick = {this.handleButton.bind(this)}>
                    Smazat
                </button>
            </td>

        );
    }
}


export default DeleteButton;
