import React, { Component } from 'react';


class EditUser extends Component {

    cancelEdit()
    {
        this.props.cancelEdit();
    }

    saveChanges()
    {
        let editedUser =
            {
                name: this.name.value,
                surname: this.surname.value,
                id: this.props.user.id
            };
        this.props.saveChanges(editedUser);
    }

    render() {

        return (
            <div>
                <h2>Uprav uživatele id: {this.props.user.id}</h2>
                    <input defaultValue={this.props.user.name} ref={(name) => { this.name = name }} />
                    <input defaultValue={this.props.user.surname} ref={(surname) => { this.surname = surname }} />
                    <button onClick={this.saveChanges.bind(this)}>
                        Uložit
                    </button>
                    <button onClick={this.cancelEdit.bind(this)}>
                        Zrušit
                    </button>
            </div>

        );
    }
}

export default EditUser;
