import React, { Component } from 'react';
import Header from './Header';
import TableBody from './TableBody';
import PropTypes from 'prop-types';
import AddUser from "./AddUser";


class Table extends Component {

    deleteRow(id)
    {
        this.props.deleteRow(id);
    }

    addUser(user)
    {
        this.props.addUser(user);
    }

    editUser(userIdToEdit)
    {
        this.props.editUser(userIdToEdit);
    }

    render() {

        return (
            <div>
                <table className="table table-striped">
                    <Header/>
                    <TableBody editUser={this.editUser.bind(this)} deleteRow={this.deleteRow.bind(this)} data={this.props.data}/>
                </table>
                <AddUser addUser={this.addUser.bind(this)}/>
            </div>
        );
    }
}

Table.propTypes = {
    data: PropTypes.array
};

export default Table;
