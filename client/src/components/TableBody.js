import React, { Component } from 'react';
import TableRow from './TableRow';
import PropTypes from 'prop-types';

class TableBody extends Component {

    deleteRow(id)
    {
        this.props.deleteRow(id);
    }

    editUser(userIdToEdit)
    {
        this.props.editUser(userIdToEdit);
    }

    render() {

        let users = this.props.data.map(user =>{
                return (
                    <TableRow editUser={this.editUser.bind(this)} deleteRow={this.deleteRow.bind(this)} key={user.id} row={user} />
                )
            }

        );


        return (
            <tbody>
                {users}
            </tbody>
        );
    }
}

TableBody.propTypes = {
    data: PropTypes.array
};

export default TableBody;
