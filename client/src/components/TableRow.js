import React, { Component } from 'react';
import TableCell from './TableCell';
import PropTypes from 'prop-types';
import DeleteButton from "./DeleteButton";

class TableRow extends Component {

    constructor(props)
    {
        super(props);
        this.state = this.props.row;
    }

    deleteRow()
    {
        this.props.deleteRow(this.props.row['id']);
    }

    editUser()
    {
        this.props.editUser(this.state['id']);
    }

    render() {

        return (
            <tr>
                <TableCell cell={this.props.row['id']}/>
                <TableCell cell={this.props.row['name']}/>
                <TableCell cell={this.props.row['surname']}/>
                <DeleteButton deleteRow={this.deleteRow.bind(this)}/>
                <td>
                    <button onClick={this.editUser.bind(this)}>Upravit</button>
                </td>
            </tr>
        );
    }
}

TableRow.propTypes = {
    row: PropTypes.object
};

export default TableRow;
