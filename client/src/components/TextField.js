import React, { Component } from 'react';


class TextField extends Component {

    onChange(){

        let data = {
            col: this.props.col,
            value: this.input.value
        };

        this.props.onChange(data);
    }

    render() {

        return (
            <input placeholder={this.props.value} type="text" ref={(input) => { this.input = input }} onChange={this.onChange.bind(this)}/>
        );
    }
}

export default TextField;
