Server se spouští pomocí příkazu "mvn spring-boot:run".

Informace o DB se nachází v src/main/resources/application.properties.

Zabezpečený end-point pro mazání uživatelů je připraven pro 3 různé varianty.
    1.) Přidaný uživatel má práva okamžitě po přidání a NENÍ nutné se přihlásit.
    2.) Přiadný uživatel je naveden do paměti, ale je nutné se přihlásit.
    3.) Práva mají všichni uživatelé v DB (vyžaduje další tabulku)

Aktuálně je v provozu varianta 2 a zbytek je zakomentován.
Dále chybí kontrola na unikátnost uživatelského jméno při přidávání do DB.