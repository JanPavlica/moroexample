package moro.server.controller;

import moro.server.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for secured URLs.
 */
@RestController
public class DeleteController {

    @Autowired
    private IUserService userService;

    /**
     * Delete end-point.
     * @param id Users ID
     * @return HttpStatus 204
     */

    /* Changed from DELETE to GET because of csrf problems in Spring Security */
    @RequestMapping("/secured/deleteUser/{id}")
    public ResponseEntity<Void> deleteArticle( @PathVariable("id") Integer id) {
        userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
