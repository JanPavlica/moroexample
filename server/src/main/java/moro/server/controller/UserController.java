package moro.server.controller;

import moro.server.entity.UserEntity;
import moro.server.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller for not secured requests.
 */
@RestController
public class UserController {

    @Autowired
    private IUserService userService;

    private final InMemoryUserDetailsManager inMemoryUserDetailsManager;

    @Autowired
    public UserController(InMemoryUserDetailsManager inMemoryUserDetailsManager) {
        this.inMemoryUserDetailsManager = inMemoryUserDetailsManager;
    }

    // all methods changed to RequestMapping (instead of Get/Post/PutMapping

    /**
     * End-point for getting user info based on ID.
     * @param id Users ID.
     * @return HTTP 200 and user info if user found. HTTP 404 and text message otherwise.
     */
    @RequestMapping("/user/{id}")
    public ResponseEntity userId(@PathVariable("id") int id) {
        UserEntity user = userService.getUserById(id);
        if(user != null)
            return ResponseEntity.status(HttpStatus.OK).body(user);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not found!");
    }

    /**
     * End-point for getting list of all users in DB.
     * @return List of users.
     */
    @RequestMapping("users")
    public ResponseEntity<List<UserEntity>> getAllUsers() {
        List<UserEntity> list = userService.getAllUsers();
        return new ResponseEntity<List<UserEntity>>(list, HttpStatus.OK);
    }

    /**
     * End-point for adding user into DB. Every newly added user has rights to delete records from table after login.
     * Missing unique username control.
     * @param user Needed informations about user.
     * @return HTTP 201 and info about new user.
     */
    @RequestMapping("/user/adduser")
    public ResponseEntity addUser(@RequestBody UserEntity user) {

        UserEntity newUser = userService.addUser(user);

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

        /*
        // After adding user is user entitled to delete immediately even WITHOUT login first!

        UserDetails userToAdd = new User(newUser.getUsername(), "{noop}"+newUser.getPassword(), authorities);
        Authentication authentication = new UsernamePasswordAuthenticationToken(userToAdd, null, authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        */

        // simple control for username duplicity, just for functionality
        if(!inMemoryUserDetailsManager.userExists(newUser.getUsername()))
            inMemoryUserDetailsManager.createUser(new User(newUser.getUsername(), "{noop}"+newUser.getPassword(), authorities));

        return ResponseEntity.status(HttpStatus.CREATED).body(newUser);
    }

    /**
     * End-point for editing user.
     * @param user Edited informations about user.
     * @return HTTP 200 and info about updated user.
     */
    @RequestMapping("/user/edit")
    public ResponseEntity updateUser(@RequestBody UserEntity user) {
        UserEntity updatedUser = userService.updateUser(user);
        return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
    }


}
