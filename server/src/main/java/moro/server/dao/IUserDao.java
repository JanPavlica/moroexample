package moro.server.dao;

import moro.server.entity.UserEntity;

import java.util.List;

public interface IUserDao {
    UserEntity getUserById(int userId);
    List<UserEntity> getAllUsers();
    UserEntity addUser(UserEntity user);
    UserEntity updateUser(UserEntity user);
    void deleteUser(int userId);

}
