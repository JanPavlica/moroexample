package moro.server.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import moro.server.entity.UserEntity;
import java.util.List;


@Transactional
@Repository
public class UserDao implements IUserDao{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public UserEntity getUserById(int userId) {
        return entityManager.find(UserEntity.class, userId);
    }


    @Override
    @SuppressWarnings("unchecked")
    //PostgreSQL error.
    public List<UserEntity> getAllUsers() {
        String hql = "FROM UserEntity as us ORDER BY us.id";
        return (List<UserEntity>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public UserEntity addUser(UserEntity user) {

        entityManager.persist(user);
        return user;
    }

    @Override
    public UserEntity updateUser(UserEntity user) {

        UserEntity preEdited = getUserById(user.getId());
        user.setPassword(preEdited.getPassword());
        user.setUsername(preEdited.getUsername());
        return entityManager.merge(user);

    }

    @Override
    public void deleteUser(int userId) {
        entityManager.remove(getUserById(userId));
    }


}
