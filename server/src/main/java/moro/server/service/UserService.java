package moro.server.service;
import moro.server.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import moro.server.dao.IUserDao;

import java.util.List;

@Service
public class UserService implements IUserService {
    @Autowired
    private IUserDao userDAO;

    @Override
    public UserEntity getUserById(int userId) {
        UserEntity obj = userDAO.getUserById(userId);
        return obj;
    }

    @Override
    public List<UserEntity> getAllUsers() {
        return userDAO.getAllUsers();
    }

    @Override
    public UserEntity addUser(UserEntity user) {

        return userDAO.addUser(user);
    }

    @Override
    public UserEntity updateUser(UserEntity user) {
        return userDAO.updateUser(user);

    }

    @Override
    public void deleteUser(int userId) {
        userDAO.deleteUser(userId);
    }

}
